package model.database;

import model.cashRegister.CashRegister;
import model.parking.ParkingDAO;
import model.parking.ParkingSpot;
import model.vehicles.Vehicle;
import model.vehicles.VehiclesDAO;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class Database implements DatabaseInterface {

    private static Database instance;

    private VehiclesDAO vehiclesDAO;
    private ParkingDAO parkingDAO;
    private CashRegister cashRegister;



    private long parkingLengthInMinutes;

    public Database() {

    }

    public static Database getInstance() {
        if (instance == null) {
            instance = new Database();

            instance.cashRegister = new CashRegister();
            instance.parkingDAO = new ParkingDAO();
            instance.vehiclesDAO = new VehiclesDAO();
        }
        return instance;
    }

    @Override
    public void assignCarToParkingSpot(int id) {
        Optional<ParkingSpot> freeSpot = parkingDAO.getParkingSpots().stream()
                .filter(parkingSpot -> !parkingSpot.isOccupied)
                .findFirst();
        if (vehiclesDAO.isAuthorized(id) && freeSpot.isPresent()) {
            freeSpot.get().vehicleId = id;
            freeSpot.get().startTime = LocalDateTime.now();
            freeSpot.get().setOccupied(true);
        } else {
            System.out.println("No free spots");
        }
    }

    @Override
    public void removeCarFromParkingSpot(int id) {
        parkingDAO.getParkingSpots().stream()
                .filter(parkingSpot -> parkingSpot.vehicleId == id)
                .forEach(parkingSpot -> {
                    parkingSpot.endTime = LocalDateTime.now();
                    countParkingLength(parkingSpot.startTime, parkingSpot.endTime);
                    cashRegister.countCharge(getPricePerHourForVehicle(id), parkingLengthInMinutes);
                    parkingSpot.setOccupied(false);
                    parkingSpot.startTime = null;
                    parkingSpot.vehicleId = 0;
                });
    }

    private double getPricePerHourForVehicle(int id){
        return vehiclesDAO.getVehicleList().stream()
                .filter(vehicle -> vehicle.id == id)
                .findFirst()
                .get().getPricePerHour();
    }

    @Override
    public void countParkingLength(LocalDateTime startTime, LocalDateTime endTime) {
        parkingLengthInMinutes = (ChronoUnit.SECONDS.between(startTime, endTime)) / 60;

    }


    @Override
    public VehiclesDAO getVehiclesDAO() {

        return vehiclesDAO;
    }

    @Override
    public ParkingDAO getParkingDAO() {
        return parkingDAO;
    }

    @Override
    public List<Vehicle> vehiclesInParking() {
        List<Vehicle> vehiclesInParkingList = new ArrayList<>();
        parkingDAO.getParkingSpots().stream()
                .filter(parkingSpot -> parkingSpot.isOccupied)
                .forEach(parkingSpot -> vehiclesInParkingList.add(getVehiclesDAO().getVehicleById(parkingSpot.vehicleId).get()));
        return vehiclesInParkingList;
    }

    @Override
    public List<ParkingSpot> parkingSpotsAvailable() {
        return getParkingDAO().getParkingSpots().stream()
                .filter(parkingSpot -> !parkingSpot.isOccupied)
                .collect(Collectors.toList());
    }

    @Override
    public long getParkingLengthInMinutes(){
        return parkingLengthInMinutes;
    }


}
