package model.database;

import model.parking.ParkingDAO;
import model.parking.ParkingSpot;
import model.vehicles.Vehicle;
import model.vehicles.VehiclesDAO;

import java.util.List;
import java.util.Optional;

import java.time.LocalDateTime;

public interface DatabaseInterface {


    void assignCarToParkingSpot(int id);
    void removeCarFromParkingSpot(int id);
    void countParkingLength(LocalDateTime startTime, LocalDateTime endTime);
    VehiclesDAO getVehiclesDAO();
    ParkingDAO getParkingDAO();
    List<Vehicle> vehiclesInParking();
    List<ParkingSpot> parkingSpotsAvailable();
    long getParkingLengthInMinutes();

}
