package model.cashRegister;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

public class CashRegister {


    public Map<LocalDate, Double> getRevenueByDateList() {
        return revenueByDateList;
    }

    Map<LocalDate, Double> revenueByDateList = new HashMap<>();

    private double dailyRevenue;

    public double getDailyRevenue() {
        return dailyRevenue;
    }

    public void setDailyRevenue(double dailyRevenue) {
        this.dailyRevenue = dailyRevenue;
    }

    public double countCharge(double pricePerHour, long minutes) {
        double charge;
        if (minutes % 60 != 0) {
            charge = pricePerHour * (minutes / 60) + pricePerHour;
        } else {
            charge = pricePerHour * (minutes / 60);
        }
        dailyRevenue += charge;


        return charge;
    }

    public void setRevenueByDateList(Map<LocalDate, Double> revenueByDateList) {
        this.revenueByDateList = revenueByDateList;
    }

    public void closeDailyCashRegister() {
        revenueByDateList.put(LocalDate.now(), dailyRevenue);
        dailyRevenue = 0;
    }
}
