package model.vehicles;

public class Motorcycle extends Vehicle {


    public Motorcycle() {
        this.vehicleType = "Motorcycle";
    }

    @Override
    public double getPricePerHour() {
        return 1.5d;
    }

    @Override
    public String toString() {
        return "Motorcycle{" +
                "id=" + id +
                ", authorized=" + authorized +
                ", vehicleType='" + vehicleType + '\'' +
                '}';
    }
}
