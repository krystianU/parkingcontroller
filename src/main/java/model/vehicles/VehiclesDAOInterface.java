package model.vehicles;

import java.util.Optional;

public interface VehiclesDAOInterface {

    void addAuto();
    void addTruck();
    void addMotorcycle();
    void authorizeVehicle(int id);
    void makeVehicleUnauthorised(int id);
    boolean isAuthorized(int id);
    Optional<Vehicle> getVehicleById(int id);

}
