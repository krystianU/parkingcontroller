package model.vehicles;

public abstract class Car extends Vehicle {

    public String vehicleSubType;

    public Car() {
        this.vehicleType = "Car";
    }

    public String getVehicleType() {
        return vehicleType;
    }


    public String getVehicleSubType() {
        return vehicleSubType;
    }

    public void setVehicleSubType(String vehicleSubType) {
        this.vehicleSubType = vehicleSubType;
    }


}
