package model.vehicles;

public abstract class Vehicle {

    public int id;
    public boolean authorized;
    public String vehicleType;


    public Vehicle() {
    }

    public abstract double getPricePerHour();


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isAuthorized() {
        return authorized;
    }

    public void setAuthorized(boolean authorized) {
        this.authorized = authorized;
    }

    public String getVehicleType() {
        return vehicleType;
    }


}

