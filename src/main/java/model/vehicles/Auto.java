package model.vehicles;

public class Auto extends Car {


    public Auto() {
        this.vehicleSubType = "Auto";
    }

    @Override
    public double getPricePerHour() {
        return 3d;
    }


    @Override
    public String toString() {
        return "Auto{" +
                "vehicleSubType='" + vehicleSubType + '\'' +
                ", id=" + id +
                ", authorized=" + authorized +
                ", vehicleType='" + vehicleType + '\'' +
                '}';
    }
}
