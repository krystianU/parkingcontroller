package model.vehicles;

public class Truck extends Car {

    public Truck() {
        this.vehicleSubType = "Truck";
    }

    @Override
    public double getPricePerHour() {
        return 5d;
    }


    @Override
    public String toString() {
        return "Truck{" +
                "vehicleSubType='" + vehicleSubType + '\'' +
                ", id=" + id +
                ", authorized=" + authorized +
                ", vehicleType='" + vehicleType + '\'' +
                '}';
    }
}
