package model.vehicles;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class VehiclesDAO implements VehiclesDAOInterface {

    List<Vehicle> vehicleList = new ArrayList<>();

    @Override
    public void addAuto() {
        Vehicle auto = new Auto();
        if (vehicleList.isEmpty()) {
            auto.setId(1);
        } else {
            auto.setId(vehicleList.get(vehicleList.size() - 1).getId() + 1);
        }
        vehicleList.add(auto);
    }

    @Override
    public void addTruck() {
        Vehicle truck = new Truck();
        if (vehicleList.isEmpty()) {
            truck.setId(1);
        } else {
            truck.setId(vehicleList.get(vehicleList.size() - 1).getId() + 1);
        }
        vehicleList.add(truck);
    }

    @Override
    public void addMotorcycle() {
        Vehicle motorcycle = new Motorcycle();
        if (vehicleList.isEmpty()) {
            motorcycle.setId(1);
        } else {
            motorcycle.setId(vehicleList.get(vehicleList.size() - 1).getId() + 1);
        }
        vehicleList.add(motorcycle);
    }

    @Override
    public void authorizeVehicle(int id) {
        if (vehicleList.stream().noneMatch(vehicle -> vehicle.id == id)) {
            return;
        } else {
            vehicleList.stream()
                    .filter(vehicle -> vehicle.id == id)
                    .findAny()
                    .get()
                    .authorized = true;
        }

    }

    @Override
    public void makeVehicleUnauthorised(int id) {
        if (vehicleList.stream().noneMatch(vehicle -> vehicle.id == id)) {
            //to do
        } else {
            vehicleList.stream().filter(vehicle1 -> vehicle1.id == id)
                    .findAny()
                    .get()
                    .authorized = false;
        }

    }

    @Override
    public boolean isAuthorized(int id) {
        if (vehicleList.stream().filter(vehicle -> vehicle.id == id)
                .anyMatch(vehicle -> vehicle.authorized)) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public Optional<Vehicle> getVehicleById(int id) {
        return vehicleList.stream()
                .filter(vehicle -> vehicle.id == id)
                .findFirst();
    }

    public List<Vehicle> getVehicleList() {
        return vehicleList;
    }
}
