package model.parking;

import java.time.LocalDateTime;

public class ParkingSpot {

    public int id;
    public boolean isOccupied;
    public int vehicleId;
    public LocalDateTime startTime;
    public LocalDateTime endTime;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isOccupied() {
        return isOccupied;
    }

    public void setOccupied(boolean occupied) {
        isOccupied = occupied;
    }

    @Override
    public String toString() {
        return "ParkingSpot{" +
                "id=" + id +
                ", isOccupied=" + isOccupied +
                ", vehicleId=" + vehicleId +
                '}';
    }
}
