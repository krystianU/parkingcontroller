package model.parking;

import java.util.ArrayList;
import java.util.List;

public class ParkingDAO {
    List<ParkingSpot> parkingSpots = new ArrayList<>();
    static int defaultParkingSize = 200;

    public void createParking() {
        for (int i = 1; i <= defaultParkingSize; i++) {
            ParkingSpot parkingSpot = new ParkingSpot();
            parkingSpot.setId(i);
            parkingSpots.add(parkingSpot);
        }
    }

    public List<ParkingSpot> getParkingSpots() {
        return parkingSpots;
    }

    public void changeParkingSize(){

    }

}
