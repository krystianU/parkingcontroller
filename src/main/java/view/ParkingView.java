package view;

import controller.ConsoleController;
import controller.ParkingController;
import controller.ScannerController;
import model.database.Database;

public class ParkingView implements ParkingMVC.View {

    ParkingMVC.Controller parkingController = new ParkingController(Database.getInstance());
    ScannerMVC.Controller scannerController = new ScannerController();
    ConsoleMVC.Controller consoleController = new ConsoleController();
}
