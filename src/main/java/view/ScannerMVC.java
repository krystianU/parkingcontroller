package view;

public interface ScannerMVC {
    interface Controller {
        int userInputInt();
    }

    interface View {

    }
}
