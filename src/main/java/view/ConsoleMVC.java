package view;

public interface ConsoleMVC {

    interface Controller{
        void printLine(String string);
    }

    interface View{

    }
}
