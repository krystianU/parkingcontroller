package controller;

import view.ScannerMVC;

import java.util.Scanner;

public class ScannerController implements ScannerMVC.Controller {
    @Override
    public int userInputInt() {
        return new Scanner(System.in).nextInt();
    }
}
