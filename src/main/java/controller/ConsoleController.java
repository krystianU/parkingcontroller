package controller;

import view.ConsoleMVC;

public class ConsoleController implements ConsoleMVC.Controller {
    @Override
    public void printLine(String string) {
        System.out.println(string);
    }
}
