package model.parking;

import org.junit.Assert;
import org.junit.Test;
import view.ParkingMVC;

import static org.junit.Assert.*;

public class ParkingDAOTest {

    ParkingDAO test = new ParkingDAO();

    @Test
    public void createParking() {
        test.createParking();

        Assert.assertEquals(test.getParkingSpots().get(0).getId(), 1);
    }

}