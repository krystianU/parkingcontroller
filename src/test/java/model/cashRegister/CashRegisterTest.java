package model.cashRegister;

import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDate;

import static org.junit.Assert.*;

public class CashRegisterTest {

    CashRegister test = new CashRegister();

    @Test
    public void countCharge() {
        double pricePerHour = 5d;
        long minutes = 100;
        double chargeActual = pricePerHour * (minutes / 60);
        double chargeTest = test.countCharge(pricePerHour, minutes);

        Assert.assertEquals(chargeActual, chargeTest, 0);
    }

    @Test
    public void closeDailyCashRegister() {
        LocalDate localDate = LocalDate.now();
        test.countCharge(5, 100);
        test.countCharge(5, 100);
        test.countCharge(5, 100);
        test.countCharge(5, 100);
        test.countCharge(5, 100);
        test.countCharge(5, 100);
        double dailyRevenue = 100 / 60 * 5 * 6;
        test.closeDailyCashRegister();

        Assert.assertEquals(test.getRevenueByDateList().size(), 1);
        Assert.assertEquals(test.getRevenueByDateList().get(localDate), 60, 0);
    }
}