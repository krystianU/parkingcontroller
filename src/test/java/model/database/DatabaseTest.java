package model.database;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDateTime;

public class DatabaseTest {

    DatabaseInterface test = Database.getInstance();

    @Before
    public void before(){
        test.getParkingDAO().createParking();
        test.getVehiclesDAO().addAuto();
        test.getVehiclesDAO().addTruck();
        test.getVehiclesDAO().addMotorcycle();
        test.getVehiclesDAO().authorizeVehicle(1);

    }

    @Test
    public void assignCarToParkingSpot() {
        test.assignCarToParkingSpot(1);
        Assert.assertTrue(test.getParkingDAO().getParkingSpots().get(0).isOccupied);
        Assert.assertEquals(test.getParkingDAO().getParkingSpots().get(0).vehicleId, 1);
        Assert.assertNotNull(test.getParkingDAO().getParkingSpots().get(0).startTime);
    }

    @Test
    public void removeCarFromParkingSpot() {
        test.assignCarToParkingSpot(1);
        test.removeCarFromParkingSpot(1);
        Assert.assertFalse(test.getParkingDAO().getParkingSpots().get(0).isOccupied);
        Assert.assertEquals(test.getParkingDAO().getParkingSpots().get(0).vehicleId, 0);
        Assert.assertNull(test.getParkingDAO().getParkingSpots().get(0).startTime);
        Assert.assertNotNull(test.getParkingDAO().getParkingSpots().get(0).endTime);
    }

    @Test
    public void countParkingLength() {
        LocalDateTime startTime = LocalDateTime.of(2019, 12, 8,12,00);
        LocalDateTime stopTime = LocalDateTime.of(2019, 12, 8,13,30);

        test.countParkingLength(startTime, stopTime);

        Assert.assertEquals(test.getParkingLengthInMinutes(), 90);

    }

    @Test
    public void vehiclesInParking() {
        test.assignCarToParkingSpot(1);
        Assert.assertEquals(test.vehiclesInParking().size(), 1);

    }

    @Test
    public void parkingSpotsAvailable() {
        test.assignCarToParkingSpot(1);
        Assert.assertEquals(test.parkingSpotsAvailable().size(),199);
    }
}