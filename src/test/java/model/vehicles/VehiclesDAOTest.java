package model.vehicles;

import com.sun.source.tree.AssertTree;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class VehiclesDAOTest {

    private VehiclesDAO vehiclesDAOtest = new VehiclesDAO();

    @Test
    public void addAuto() {

        vehiclesDAOtest.addAuto();
        vehiclesDAOtest.addAuto();
        Assert.assertTrue(vehiclesDAOtest.getVehicleById(1).isPresent());
        Assert.assertTrue(vehiclesDAOtest.getVehicleById(2).isPresent());
        Assert.assertFalse(vehiclesDAOtest.getVehicleById(10).isPresent());

        Assert.assertTrue(vehiclesDAOtest.vehicleList.size() == 2);
        Assert.assertNotEquals(vehiclesDAOtest.vehicleList.size(),5);
    }

    @Test
    public void addTruck() {
        vehiclesDAOtest.addTruck();
        vehiclesDAOtest.addTruck();
        vehiclesDAOtest.addAuto();
        vehiclesDAOtest.addMotorcycle();
        Assert.assertTrue(vehiclesDAOtest.getVehicleById(1).isPresent());
        Assert.assertTrue(vehiclesDAOtest.getVehicleById(2).isPresent());
        Assert.assertTrue(vehiclesDAOtest.getVehicleById(3).isPresent());
        Assert.assertTrue(vehiclesDAOtest.getVehicleById(4).isPresent());
        Assert.assertFalse(vehiclesDAOtest.getVehicleById(10).isPresent());
    }

    @Test
    public void addMotorcycle() {
        vehiclesDAOtest.addMotorcycle();
        vehiclesDAOtest.addTruck();
        Assert.assertTrue(vehiclesDAOtest.getVehicleById(2).isPresent());
        Assert.assertFalse(vehiclesDAOtest.getVehicleById(10).isPresent());

    }

    @Test
    public void authorizeVehicle() {
        vehiclesDAOtest.addMotorcycle();
        vehiclesDAOtest.addAuto();
        vehiclesDAOtest.addTruck();
        vehiclesDAOtest.addTruck();


        vehiclesDAOtest.authorizeVehicle(1);
        vehiclesDAOtest.authorizeVehicle(2);
        vehiclesDAOtest.authorizeVehicle(3);

        Assert.assertTrue(vehiclesDAOtest.vehicleList.size() == 4);

        Assert.assertTrue(vehiclesDAOtest.vehicleList.stream()
        .filter(vehicle -> vehicle.id == 1)
        .findAny()
        .get()
        .authorized);

        Assert.assertTrue(vehiclesDAOtest.vehicleList.get(1).authorized);
        Assert.assertTrue(vehiclesDAOtest.vehicleList.get(2).authorized);
        Assert.assertFalse(vehiclesDAOtest.vehicleList.get(3).authorized);


    }

    @Test
    public void makeVehicleUnauthorised() {

        vehiclesDAOtest.addMotorcycle();
        vehiclesDAOtest.addAuto();
        vehiclesDAOtest.addTruck();
        vehiclesDAOtest.addTruck();


        vehiclesDAOtest.authorizeVehicle(1);
        vehiclesDAOtest.authorizeVehicle(2);
        vehiclesDAOtest.authorizeVehicle(3);

        Assert.assertTrue(vehiclesDAOtest.vehicleList.size() == 4);

        Assert.assertTrue(vehiclesDAOtest.vehicleList.stream()
                .filter(vehicle -> vehicle.id == 1)
                .findAny()
                .get()
                .authorized);

        vehiclesDAOtest.makeVehicleUnauthorised(1);

        assertFalse(vehiclesDAOtest.vehicleList.stream()
                .filter(vehicle -> vehicle.id == 1)
                .findAny()
                .get()
                .authorized);

    }

    @Test
    public void isAuthorized() {
        vehiclesDAOtest.addTruck();
        vehiclesDAOtest.addTruck();
        vehiclesDAOtest.authorizeVehicle(1);

        Assert.assertTrue(vehiclesDAOtest.isAuthorized(1));
        Assert.assertFalse(vehiclesDAOtest.isAuthorized(2));
    }

}